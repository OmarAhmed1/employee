﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace EmployeeApiDB.Models
{
    public class EmployeeApiContext : DbContext
    {
        public EmployeeApiContext() : base() { }
        public EmployeeApiContext(DbContextOptions<EmployeeApiContext> options)
         : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<EmployeeSkill> EmployeeSkills { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Skill>().HasData(new Skill { ID = 1, skill = "Python" },
                new Skill { ID = 2, skill = "Node Js" },
                new Skill { ID = 3, skill = "Java" },
                new Skill { ID = 4, skill = "PHP" },
                new Skill { ID = 5, skill = "Django" },
                new Skill { ID = 6, skill = "Angular" },
                new Skill { ID = 7, skill = "Vue" },
                new Skill { ID = 8, skill = "ReactJs" });
        }
    }
}
