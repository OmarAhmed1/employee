﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeApiDB.Models
{
    public class Skill
    {
        public int ID { get; set; }

        [Required]
        public string skill { get; set; }

        public  List<EmployeeSkill> EmployeeSkills { get; set; } 
    }
}
