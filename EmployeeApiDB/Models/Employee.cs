﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeApiDB.Models
{
    public class Employee
    {
        public int ID { get; set; }

        [Required]
        public string EmployeeName { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public  List<EmployeeSkill> EmployeeSkills { get; set; } 

        public Employee()
        {
            EmployeeSkills = new List<EmployeeSkill>();
        }
    }
}
