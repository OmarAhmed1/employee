﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;
namespace EmployeeApiDB.Models
{
    public class EmployeeSkill
    {
        public int ID { get; set; }

        public int EmployeeID { get; set; }

        public int SkillID { get; set; }


        public  Employee Employee { get; set; } 
        public Skill Skill { get; set; } 

    }
}
