﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeeApiDB.Models;

namespace EmployeeApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly EmployeeApiContext _context;

        public EmployeesController(EmployeeApiContext context)
        {
            _context = context;
        }

        // GET: api/Employees
        [HttpGet]
        public IActionResult GetEmployees()
        {
            List<Employee> employees = _context.Employees.Include(e => e.EmployeeSkills).ToList();
            return Ok(employees);
        }

        // GET: api/Employees/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Employee>> GetEmployee(int id)
        {
            var employee = await _context.Employees.FindAsync(id);

            if (employee == null)
            {
                return NotFound();
            }

            return employee;
        }

        // PUT: api/Employees/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmployee(int id, Employee employee)
        {
            if (id != employee.ID)
            {
                return BadRequest();
            }
            

            List<EmployeeSkill> empsk = _context.EmployeeSkills.Where(es => es.EmployeeID == id).ToList();

            if (employee.EmployeeSkills.Count == 0)
            {

                foreach (EmployeeSkill es in empsk)
                {
                    _context.EmployeeSkills.Remove(es);
                }
                

            }
            else
            {

                foreach (EmployeeSkill es in empsk)
                {
                   EmployeeSkill test = employee.EmployeeSkills.FirstOrDefault(e=> e.ID == es.ID); //empsk.ToList()
                    if (test == null)
                    {
                        _context.EmployeeSkills.Remove(es);
                    }
                }

                    foreach (EmployeeSkill es in employee.EmployeeSkills.ToList())
                {
                    var empskill = _context.EmployeeSkills.Find(es.ID);

                    if (empskill == null)
                    {
                        EmployeeSkill NewEs = new EmployeeSkill();
                        NewEs.EmployeeID = employee.ID;
                        NewEs.SkillID = es.SkillID;
                        _context.EmployeeSkills.Add(NewEs);
                        _context.SaveChanges();
                    }
                    else
                    {
                        var empskil = _context.EmployeeSkills.Find(es.ID);
                        _context.Entry<EmployeeSkill>(empskil).State = EntityState.Detached;
                        _context.Attach(es);
                        _context.Entry(es).State = EntityState.Modified;
                    }
                }
            }

            _context.Entry(employee).State = EntityState.Modified;


            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Employees
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Employee>> PostEmployee(Employee employee)
        {
            _context.Employees.Add(employee);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEmployee", new { id = employee.ID }, employee);
        }

        // DELETE: api/Employees/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Employee>> DeleteEmployee(int id)
        {
            var employee = await _context.Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }

            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();

            return employee;
        }

        private bool EmployeeExists(int id)
        {
            return _context.Employees.Any(e => e.ID == id);
        }
    }
}
