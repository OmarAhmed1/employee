import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EmitterVisitorContext } from '@angular/compiler';
import { Employee } from '../employees/employee.model';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';
@Injectable({providedIn: 'root'}) 
export class EmpService{
      
      EmpChanges = new Subject<Employee[]>();
      SkillChanges = new Subject<any[]>();
      URL:string= "https://localhost:44321/api/Employees";
      URLSK:string= "https://localhost:44321/api/Skills";
      EmpArray: Employee[];
      SkArr: any[];

     constructor(private http: HttpClient){} 
     PostEmp(emp: Employee){  
        this.http.post<Employee>(this.URL, emp).subscribe(
            response => { console.log(response); });
     }

     FetchEmps(){  
        this.http.get<Employee[]>(this.URL)
        .subscribe(emps => {
            console.log(emps);
            this.SetEmpList(emps);
        }) 
    }
    FetchSkills(){ 
      this.http.get<Employee[]>(this.URLSK)
      .subscribe(sk => {
          console.log(sk);
          this.SetSkillList(sk);
      }) 
  }

    SetEmpList(arr: Employee[]){
        this.EmpArray = arr;
        this.EmpChanges.next(this.EmpArray);
    }
    SetSkillList(arrsk: any[]){
      this.SkArr = arrsk;
      this.SkillChanges.next(this.SkArr);
  }

    DeleteEmp(id: number){ 
        let URLd:string="https://localhost:44321/api/Employees/"+id
        this.http.delete(URLd).subscribe(
         response => { console.log(response); });
      }

      EditEmp(empe: Employee){
        let URLE:string="https://localhost:44321/api/Employees/"+empe.ID
        this.http.put(URLE,empe).subscribe(
          response => { console.log(response); });
       console.log(empe);
       }
}