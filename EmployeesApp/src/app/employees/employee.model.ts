

export class Employee {
  public ID: number;
  public EmployeeName: string;
  public Email: string;
  public EmployeeSkills: any[];

  constructor(EmployeeSkills: any[] ) {
    this.EmployeeSkills = EmployeeSkills;
  }
}