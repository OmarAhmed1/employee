import { Component, OnInit } from '@angular/core';
import { Employee } from '../employee.model';
import { NgFormSelectorWarning, NgForm } from '@angular/forms';
import { EmpService } from 'src/app/shared/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  editCustomer = false;
  empBind: Employee=new Employee([]);
  editMode: boolean =  false;


  ShowRegForm(e){
    this.editCustomer=true;  
  }

  onNew(){
    this.empBind =new Employee([{}]);
    this.ShowRegForm(this.empBind);
    this.editMode = false;
    this.selected = [];
  }

  onEdit(em: Employee){
       this.empBind=em;
       this.selected = [];
      //this.empsr.EditEmp(em);
      this.editMode = true;

      for (let value of this.empBind.EmployeeSkills) {
      this.selected.push(value.SkillID)
      }
      this.ShowRegForm(this.empBind);
  }

  onDelete(em: Employee, index: number){
    this.empsr.DeleteEmp(em.ID);
    this.employeelist.splice(index,1);
 }
 onCancel(){
  this.editCustomer = false;
 }

  onSubmit(form: NgForm){
    if(this.editMode){
       let num = this.empBind.EmployeeSkills.length;
       let counter=0;
     for (let i in this.selected) 
     { 
        counter=counter+1;
        if(counter<=num){
          this.SkillArr.push({"ID":this.empBind.EmployeeSkills[i].ID,"EmployeeID":this.empBind.ID,"SkillID": this.selected[i]});//,"EmployeeID":this.empBind.ID,"SkillID": this.selected[i]});
        }else{
          this.SkillArr.push({"ID":-1,"EmployeeID":this.empBind.ID,"SkillID": this.selected[i]});
        }
     }
      this.empBind.EmployeeSkills = this.SkillArr;

      this.empsr.EditEmp(this.empBind);
    }
    else{
    
    for (let value of this.selected) { 
      this.SkillArr.push({SkillID: value});   
       }  
      this.empBind.EmployeeSkills = this.SkillArr;
    this.empsr.PostEmp(this.empBind);
    this.employeelist.push(this.empBind);
    }
  }
  employeelist: Employee[];
  items = [];
  selected = [];
  SkillArr = [];
  getValues() {
    console.log(this.selected);
    console.log(this.empBind.ID);
  }

  constructor(private empsr: EmpService) { }

  ngOnInit() {
    
    this.empsr.FetchEmps();
    this.empsr.FetchSkills();
    this.empsr.EmpChanges.subscribe(
      (x: Employee[]) => {
        this.employeelist = x;
      }
    )
    
    this.empsr.SkillChanges.subscribe(
      (xy: any[]) => {
        this.items = xy;
      }
    )
  }

}
