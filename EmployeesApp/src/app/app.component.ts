import { Component } from '@angular/core';
import { EmpService } from './shared/employee.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [EmpService]
})
export class AppComponent {
  title = 'EmployeesApp';
}
